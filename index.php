<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <h1>Du är inloggad</h1>
    <?php
    session_start();

    if(!isset($_SESSION['login'])){
      header("Location: login.php");
    }

    ?>

    <form method="post">
      <input name="logout" type="submit" value="logout">
    </form>

    <?php

    if(isset($_POST['logout'])) {
      unset($_SESSION['login']);
      header("Location: login.php");
    }


     ?>
  </body>
</html>
